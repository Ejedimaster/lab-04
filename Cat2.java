public class Cat2 
{
	private int weight;
	private int age;
	public String name;
	//constructor methods
	
	public Cat2(int weight, int age, String name)
	{
		this.weight = weight;
		this.age = age;
		this.name = name;
	}
	
	public int getWeight()
	{
		return this.weight;
	}
	
	public void setWeight(int newWeight)
	{
		this.weight = newWeight;
	}
	
	public String getName()
	{
		return this.name;
	}
	public void setName(String newName)
	{
		this.name = newName;
	}
	
	public int getAge()
	{
		return this.age;
	}
	
	public void setAge(int newAge)
	{
		this.age = newAge;
	}
	
	public void speak()
	{
		System.out.println(name + " says hi!");
	}
	
	public void whatWeight()
	{
		System.out.println(name + " is " + weight + " pounds!");
	}
	
	public void doJump() 
	{
		boolean doJump = HelpJump();
		System.out.println("Can " + this.name + " Jump?: " + doJump);
	}
	
	private boolean HelpJump()
	{
		boolean doJump;
		
		if (this.weight > 10) 
		{
			doJump = false;
		}
		else
		{
			doJump = true;
		}
		
		return doJump;
		
	}
	
	
}